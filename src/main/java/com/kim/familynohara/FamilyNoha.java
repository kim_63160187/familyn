/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.familynohara;

/**
 *
 * @author ASUS
 */
public class FamilyNoha {
    // กำหนดคุณสมบัติ
    protected String name;
    protected int age;
    protected String sex;
    protected String job;
    
//กำหนดค่าตัวแปรให้เป็นconstructor
    FamilyNoha(String name,int age,String sex, String job){
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.job = job;
    }
    
    //กำหนดเมธอดเพื่ออธิบายข้อมูล
    public void getDetails( ) {
        System.out.println("Name :" + " "+ name );
        System.out.println("Age :" + " "+" age" + "years" );
        System.out.println("Sex :" +" "+ sex);
        System.out.println("Job :" + " "+ job);
        
     }
    
}
